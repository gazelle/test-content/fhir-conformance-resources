# fhir-conformance-resources

Contains the FHIR structure definitions, code systems, value sets and other FHIR conformance resources required to validate FHIR based implementations

# Directory's structure

* CodeSystem (The CodeSystem resources)
* StructureDefinition (The StructureDefinition resources)
* ValueSet (The ValueSet resources)

In each directory, one directory per project (IHE, EPR etc)

# IHE conformance resources

The Conformance resources in the master branch are those coming from IHE International GitHub project hosted at [https://github.com/IHE/fhir](https
://github.com/IHE/fhir).


# Testing

For each StructureDefinition, we want to have sample resources to be able to test the correctness of the StructureDefinition. Keep the same directory structure in the samples and scripts
directory as in the directory where the structure definition files are stored.